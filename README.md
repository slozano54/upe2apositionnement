# UPE2A - Tests de positionnement
Retranscription LaTeX des tests de positionnement du [CASNAV de Lille](https://casnav.site.ac-lille.fr/) pour les allophones.

[Vers la page des tests](https://casnav.site.ac-lille.fr/evaluation-positionnement-en-mathematiques/)
