    {\sc\textbf{CE2 vers CM1}}\par\smallskip
    \begin{minipage}{0.2\linewidth}
        \faEye\\[2mm]
        \Multiplication[Solution]{436}{12}
        \hspace*{1mm}
        \rule{1mm}{35mm}
    \end{minipage}
    \begin{minipage}{0.75\linewidth}
        \faPen\\[2mm]
        \Addition[Solution]{371}{528} \hfill \Addition[Solution]{168}{53} \hfill \Soustraction[Solution]{74}{53} \hfill \Soustraction[Solution]{513}{59} \hfill \Multiplication[Solution]{645}{32}
    \end{minipage}
