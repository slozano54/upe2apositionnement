    {\sc\textbf{6\up{e} vers 5\up{e}}}\par\smallskip
    \faPen \\[2mm]
    $\dfrac{20}{30}=\dfrac{\mathcolor{red}{\num{2}}}{\mathcolor{red}{\num{3}}}$ \hfill $\dfrac{25}{10}=\mathcolor{red}{\num{2.5}}$ \hfill$\dfrac{875}{\mathcolor{red}{\num{100}}}=\num{8.75}$ \\[5mm]
    $\dfrac{20}{16}=\dfrac{\mathcolor{red}{\num{4}}\times\mathcolor{red}{\num{5}}}{\mathcolor{red}{\num{4}}\times\mathcolor{red}{\num{4}}}=\dfrac{\mathcolor{red}{\num{5}}}{\mathcolor{red}{\num{4}}}$ \hfill $\dfrac{3\times 4}{4\times 3}=\dfrac{\mathcolor{red}{\num{12}}}{\mathcolor{red}{\num{12}}}=\mathcolor{red}{\num{1}}$
