    {\sc\textbf{CM2 vers 6\up{e}}}\par\smallskip
    \begin{minipage}{0.25\linewidth}
        \faEye\\[2mm]
        \Division[Solution]{1470}{42}
        \hspace*{1mm}
        \rule{1mm}{30mm}
    \end{minipage}
    \begin{minipage}{0.7\linewidth}
        \faPen\\[2mm]
        $\num{374}+\num{21.65}+\num{4.35}=\mathcolor{red}{\num{400}}$ \hfill $\num{378.40}-\num{77.90}=\mathcolor{red}{\num{300.5}}$ \hfill $\num{327}\div\num{6}=\mathcolor{red}{\num{54.5}}$\\[4mm]
        $\num{593}\times\num{0.01}=\mathcolor{red}{\num{5.93}}$ \hfill $\num{56}\div\num{8}=\mathcolor{red}{\num{7}}$ \hfill \Division[Solution]{327}{6}
    \end{minipage}
