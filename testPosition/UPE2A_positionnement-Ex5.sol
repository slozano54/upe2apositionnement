    {\sc\textbf{CM1 vers CM2}}\par\smallskip
    \begin{minipage}{0.2\linewidth}
        \faEye\\[2mm]
        \Addition[Solution]{25.5}{76.32}
        \hspace*{1mm}
        \rule{1mm}{30mm}
    \end{minipage}
    \begin{minipage}{0.75\linewidth}
        \faPen\\[2mm]
        $\num{4376}+\num{5623}=\mathcolor{red}{\num{9999}}$ \hfill $\num{76.50}+\num{0.50}=\mathcolor{red}{\num{77}}$ \hfill \phantom{$\num{76.50}+\num{0.50}=\mathcolor{red}{\num{77}}$}\\[4mm]
        $\num{7965}-\num{5623}=\mathcolor{red}{\num{2342}}$ \hfill $\num{575.50}-\num{25.50}=\mathcolor{red}{\num{550}}$ \hfill $\num{7.25}+\num{100}=\mathcolor{red}{\num{725}}$
    \end{minipage}
