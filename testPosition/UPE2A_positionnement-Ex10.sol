    {\sc\textbf{3\up{e} vers 2\up{de}}}\par\smallskip
    \faPen \\[2mm]
    $1+3a-b-1-2a+2b=\mathcolor{red}{a+b}$ \hfill $(x-3)^2=\mathcolor{red}{x^2-6x+9}$ \hfill$(\sqrt{2})^4=\mathcolor{red}{4}$ \\[5mm]
    \systeme{x+2y=0,x+y=1} \hspace*{10mm} $\longmapsto$ $x=\mathcolor{red}{2}$; $y=\mathcolor{red}{-1}$; \\[5mm]
    $(x-1)(x+2)=0$ \hspace*{10mm} $\longmapsto$ $x=\mathcolor{red}{1}$; $x=\mathcolor{red}{-2}$;
