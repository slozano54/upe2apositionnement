    {\sc\textbf{GS vers CP}}\par\smallskip
    \begin{minipage}{0.2\linewidth}
        \faEye\\[2mm]
        \begin{tikzpicture}
            \node at (1,0.5) {\emoji{black-heart}\emoji{black-heart}\emoji{black-heart}};
            \draw[rounded corners=10pt] (0,0) rectangle ++(2,1);
            \draw (1,0) -- (1,-0.5);
            \draw (1,-0.8) circle(0.3);
            \node at (1,-0.8) {\bfseries 3};
        \end{tikzpicture}
        \hspace*{1mm}
        \rule{1mm}{30mm}
    \end{minipage}
    \begin{minipage}{0.45\linewidth}
        \faPen\\
        \begin{tikzpicture}
            \node at (1.5,0.5) {\emoji{black-heart}\emoji{black-heart}\emoji{black-heart}\emoji{black-heart}\emoji{black-heart}\emoji{black-heart}};
            \draw[rounded corners=10pt] (0,0) rectangle ++(3,1);
            \draw (1.5,0) -- (1.5,-0.5);
            \draw (1.5,-0.8) circle(0.3);
            \node at (1.5,-0.8) {\bfseries\textcolor{red}{6}};
        \end{tikzpicture}
        \hfill
        \begin{tikzpicture}
            \node at (1,1.1) {\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}};
            \node at (1,0.5) {\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}};
            \draw[rounded corners=10pt] (0,0) rectangle ++(2,1.5);
            \draw (1,0) -- (1,-0.5);
            \draw (1,-0.8) circle(0.3);
            \node at (1,-0.8) {\bfseries\textcolor{red}{7}};
        \end{tikzpicture}
        \hfill
        \begin{tikzpicture}
            \node at (1,1.7) {\emoji{diamond-suit}\emoji{diamond-suit}\emoji{diamond-suit}};
            \node at (1,1.1) {\emoji{diamond-suit}\emoji{diamond-suit}\emoji{diamond-suit}};
            \node at (1,0.5) {\emoji{diamond-suit}\emoji{diamond-suit}\emoji{diamond-suit}};
            \draw[rounded corners=10pt] (0,0) rectangle ++(2,2.1);
            \draw (1,0) -- (1,-0.5);
            \draw (1,-0.8) circle(0.3);
            \node at (1,-0.8) {\bfseries\textcolor{red}{9}};
        \end{tikzpicture}
    \end{minipage}
    \begin{minipage}{0.3\linewidth}
        \begin{tikzpicture}
            \node at (2.25,1.1) {\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}};
            \node at (2.25,0.5) {\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\phantom{\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}\emoji{spade-suit}}};
            \draw[rounded corners=10pt] (0,0) rectangle ++(4.5,1.5);
            \draw (4.5,0.75) -- (5,0.75);
            \draw (5.3,0.75) circle(0.3);
            \node at (5.3,0.75) {\bfseries\textcolor{red}{13}};
        \end{tikzpicture}
        \par\medskip
        \begin{tikzpicture}
            \node at (2.25,1.1) {\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}};
            \node at (2.25,0.5) {\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}\emoji{club-suit}};
            \draw[rounded corners=10pt] (0,0) rectangle ++(4.5,1.5);
            \draw (4.5,0.75) -- (5,0.75);
            \draw (5.3,0.75) circle(0.3);
            \node at (5.3,0.75) {\bfseries\textcolor{red}{20}};
        \end{tikzpicture}
    \end{minipage}
