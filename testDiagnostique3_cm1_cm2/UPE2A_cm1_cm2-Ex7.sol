    \faPen\vspace*{-5mm}
    \begin{multicols}{4}
        \begin{itemize}
            \item[] 1 h=\textcolor{red}{60}~min;\medskip
            \item[] \Lg[km]{1}=\textcolor{red}{\Lg[m]{1000}};\medskip
            \item[] \Masse[t]{1}=\textcolor{red}{\Masse[kg]{1000}};
            \columnbreak
            \item[] 1 min=\textcolor{red}{60}~s;\medskip
            \item[] \Lg[m]{1}=\textcolor{red}{\Lg[cm]{100}};\medskip
            \item[] \Masse[kg]{1}=\textcolor{red}{\Masse[g]{1000}};
            \columnbreak
            \item[] \medskip
            \item[] \Lg[cm]{1}=\textcolor{red}{\Lg[mm]{10}};\medskip
            \item[] \Masse[g]{1}=\textcolor{red}{\Masse[mg]{1000}};
            \columnbreak
            \item[] \medskip
            \item[] \Lg[m]{50000}=\textcolor{red}{\Lg[km]{50}};\medskip
            \item[] \Masse[g]{3000}=\textcolor{red}{\Masse[kg]{3}};
        \end{itemize}
    \end{multicols}
    \bigskip
