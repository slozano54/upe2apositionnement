    \faPen\vspace*{-5mm}
    \begin{multicols}{3}
        \begin{itemize}
            \item[] $\num{275}\div\num{100}   =\mathcolor{red}{\num{2.75}}$\medskip
            \item[] $\num{2742}\div\num{3}    =\mathcolor{red}{\num{914}}$
            \columnbreak
            \item[] $\num{72}\div\num{8}     =\mathcolor{red}{\num{9}}$\medskip
            \item[] $\num{185}\div\num{37}   =\mathcolor{red}{\num{5}}$
            \columnbreak
            \item[] $\num{3230}\div\num{68}=\mathcolor{red}{\num{47.5}}$
        \end{itemize}
    \end{multicols}
    \bigskip
