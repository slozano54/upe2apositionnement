    \begin{minipage}{0.6\linewidth}
        \faEye \\
        \begin{Geometrie}[CoinHD={(10u,6.5u)}]
            trace feuillet;
            pair gridx[],gridy[];
            gridx0=(4u,u);
            gridy0=(4u,u);
            for i:=1 upto 6:
                gridx[2*i]-gridx0=u*(i,0);
                gridx[2*i-1]-gridx0=u*(i-1,5);
                draw gridx[2*i-2]--gridx[2*i-1];
                gridy[2*i]-gridy0=u*(0,i);
                gridy[2*i-1]-gridy0=u*(5,i-1);
                draw gridy[2*i-2]--gridy[2*i-1];
            endfor;
            label(btex \textbf{F} etex,(4.5u,1.5u));
            label(btex \textbf{E} etex,(4.5u,4.5u));
            label(btex \textbf{A} etex,(5.5u,3.5u));
            label(btex \textbf{C} etex,(6.5u,5.5u));
            label(btex \textbf{D} etex,(7.5u,2.5u));
            label(btex \textbf{B} etex,(8.5u,4.5u));
            for i:=4 upto 8:
                label(TEX("\textbf{"&decimal(i-3)&"}"),u*(i+0.5,0.5));
                label(TEX("\textbf{"&decimal(i-3)&"}"),u*(3.5,i-2.5));
            endfor;
            label(btex \Large$\mathbf{A(2;3)}$ etex,u*(1.5,3.5));
        \end{Geometrie}
        \rule{1mm}{60mm}
    \end{minipage}
    \hspace*{5mm}
    \begin{minipage}{0.35\linewidth}
        \vspace*{-20mm}
        \faPen \\[10mm]
        $B(\mathcolor{red}{5};\mathcolor{red}{4});$\hfill$C(\mathcolor{red}{3};\mathcolor{red}{5});$\\[5mm]
        $D(\mathcolor{red}{4};\mathcolor{red}{2});$\hfill$E(\mathcolor{red}{1};\mathcolor{red}{4});$\\[5mm]
        $F(\mathcolor{red}{1};\mathcolor{red}{1}).$
    \end{minipage}
    \bigskip
