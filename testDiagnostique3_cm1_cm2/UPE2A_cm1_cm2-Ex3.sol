    \faPen\vspace*{-5mm}
    \begin{multicols}{3}
        \begin{itemize}
            \item[] $\num{37.8}-\num{5}=\mathcolor{red}{\num{32.8}}$\medskip
            \item[] $\num{38.76}-\num{32.14}=\mathcolor{red}{\num{6.62}}$
            \columnbreak
            \item[] $\num{49}-\num{6.5}=\mathcolor{red}{\num{42.5}}$\medskip
            \item[] $\num{349.6}-\num{27.85}=\mathcolor{red}{\num{321.75}}$
            \columnbreak
            \item[] $\num{872}-\num{86.14}=\mathcolor{red}{\num{785.86}}$
        \end{itemize}
    \end{multicols}
    \bigskip
