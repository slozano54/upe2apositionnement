    \begin{minipage}{0.35\linewidth}
        \faEye\hfill $1 > \num{0.1}$; \hfill $\num{20.01} < \num{20.10}$.
    \end{minipage}
    \hspace*{3mm}
    \begin{minipage}{0.6\linewidth}
        \faPen\hfill$\num{1}~\textcolor{red}{>}~\num{0.9}$; \hfill $\num{7.9}~\textcolor{red}{<}~\num{8.1}$; \hfill $\num{0.1}~\textcolor{red}{<}~\num{1}$; \hfill $\num{0.2}~\textcolor{red}{>}~\num{0.09}$; \hfill $\num{9.5}~\textcolor{red}{=}~\num{9.5}$.
    \end{minipage}
    \bigskip
