    \faPen  \hfill%
    \setcounter{mulxlop}{0}%
    \opmul[decimalsepsymbol={,},lineheight=2em,columnwidth=1.5em,displayintermediary=none,voperator=bottom,%
        intermediarystyle=\stepcounter{mulxlop}\color{white}\PfCchiffre{A\themulxlop},%
        resultstyle=\stepcounter{mulxlop}\color{red}\PfCchiffre{A\themulxlop},%
        resultstyle.d=\color{PfCCouleurVirgule}%
        ]{8}{5}%
        \foreach \i in {1,...,\themulxlop}{%
            \PfCentoure{A\i}{A\i}%
        }%
    \hfill%
    \setcounter{mulxlop}{0}%
    \opmul[decimalsepsymbol={,},lineheight=2em,columnwidth=1.5em,displayintermediary=none,voperator=bottom,%
        intermediarystyle=\stepcounter{mulxlop}\color{white}\PfCchiffre{A\themulxlop},
        resultstyle=\stepcounter{mulxlop}\color{red}\PfCchiffre{A\themulxlop},%
        resultstyle.d=\color{PfCCouleurVirgule}%
        ]{7}{9}%
        \foreach \i in {1,...,\themulxlop}{%
            \PfCentoure{A\i}{A\i}%
        }%
    \hfill%
    \setcounter{mulxlop}{0}%
    \opmul[decimalsepsymbol={,},lineheight=2em,columnwidth=1.5em,displayintermediary=none,voperator=bottom,%
        intermediarystyle=\stepcounter{mulxlop}\color{white}\PfCchiffre{A\themulxlop},
        resultstyle=\stepcounter{mulxlop}\color{red}\PfCchiffre{A\themulxlop},%
        resultstyle.d=\color{PfCCouleurVirgule}%
        ]{63}{4}%
        \foreach \i in {1,...,\themulxlop}{%
            \PfCentoure{A\i}{A\i}%
        }%
    \hfill \Multiplication[Solution]{78}{96} \hfill \Multiplication[Solution]{123}{45}
    \bigskip
