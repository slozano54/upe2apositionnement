    \begin{minipage}{0.25\linewidth}
        \faEye\\
        \begin{minipage}{\linewidth}
            \begin{center}
                \Horloge[Secondes=false]{15:00}\\[2mm]
                \textbf{3h00 ou 15h00}
            \end{center}
        \end{minipage}
    \end{minipage}
    \hspace*{3mm}
    \begin{minipage}{0.7\linewidth}
        \faPen\\
        \begin{minipage}{0.3\linewidth}
            \begin{center}
                \Horloge[Secondes=false]{19:15}\\[2mm]
                \textcolor{red}{7h15 ou 19h15}
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{0.3\linewidth}
            \begin{center}
                \Horloge[Secondes=false]{16:45}\\[2mm]
                \textcolor{red}{4h45 ou 16h45}
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{0.3\linewidth}
            \begin{center}
                \Horloge[Secondes=false]{12:30}\\[2mm]
                \textcolor{red}{12h30}
            \end{center}
        \end{minipage}\\
    \end{minipage}
    \bigskip
