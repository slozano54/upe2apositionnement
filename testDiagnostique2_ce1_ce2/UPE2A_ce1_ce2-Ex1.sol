    \begin{minipage}{0.35\linewidth}
        \faEye\hfill $5 < 7$; \hfill $4 > 2$; \hfill $3 = 3$; \hfill $2 \neq 1$.
    \end{minipage}
    \hspace*{3mm}
    \begin{minipage}{0.6\linewidth}
        \faPen\hfill$7~\textcolor{red}{<}~10$; \hfill $19~\textcolor{red}{>}~9$; \hfill $95~\textcolor{red}{>}~57$; \hfill $32~\textcolor{red}{=}~32$; \hfill $501~\textcolor{red}{>}~498$.
    \end{minipage}
    \bigskip
