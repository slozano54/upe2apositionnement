    {\sc\textbf{CP vers CE1}}\par\smallskip
    \faPen
    \par\smallskip
    $2\times 2=\mathcolor{red}{4}$ \hspace*{15mm} $3\times 2=\mathcolor{red}{6}$ \hspace*{15mm} $2\times 7=\mathcolor{red}{14}$
    \par\bigskip
    $8=2\times\fbox{${\mathcolor{red}{4}}$}$ \hspace*{15mm} $12=2\times\fbox{${\mathcolor{red}{6}}$}$
    \bigskip
